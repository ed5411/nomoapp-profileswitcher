@echo off %debug%
set /a "Lines=35"
@mode con cols=80 lines=%Lines%
@title NomoApp Profile-Switcher
color f0
setlocal

:: Die Kommentierung jeder Zeile mit dem Wort bzw. der Zeichenfolge "::pause" ist zum Debuggen aufzuheben

::############################################ Setze notwendige Variablen ##############################################################

:: Setzen des aktuellen Verzeichnisses, in dem das Skript ausgeführt wird, als workdir-Variable
set workdir=%CD%
echo %workdir%

:: Setze Skriptnamen
set SkriptName=Start_NomoApp-Profileswitcher.bat

:: Setze direkten Nomoverzeichnisnamen
set dirNomo=nomo_app

:: Setze Profileverzeichnisname
set dirNomoProfileFrom=nomo_app Profil von

:: Setzen des App-Verzeichnisses zur Konfig der Nomo-App
set dirNomoAPP=%APPDATA%\com.nomo

:: Setzen des App-Verzeichnisses zum Profileswitcher
set dirNomoProfSwitcher=%dirNomoAPP%\nomo_app_ProfileSwitcher

:: Setze Pfad zur path2nomoApp.conf
set startPath2Nomo=%dirNomoProfSwitcher%\path2nomoApp.conf

:: Setze Pfad zur switcherOptions.opt
set switcherOptions=%dirNomoProfSwitcher%\switcherOptions.opt

:: Setze Bezeichnung der Datei f�r den zuletzt aktiven Nutzer
set nomoUser=%dirNomoProfSwitcher%\nomoLastUser.nom

:: Setze Bezeichnung der Profiledatei worin die Nutzernamen gespeichert werden
set nomoProfs=%dirNomoProfSwitcher%\nomoProfiles.nom
	
mkdir %dirNomoProfSwitcher%

::############################################ Hashwert dieses Skripts #################################################################
:: Hash this script and write in the hashlog
:: Pfad zur Datei, deren Hashwert berechnet werden soll
::set "filePath=%~1"

setlocal enabledelayedexpansion

set "filePath=%workdir%\%SkriptName%"

:: Überprüfen, ob der Dateipfad angegeben wurde
if "%filePath%"=="" (
    echo Bitte geben Sie den Pfad zur Datei an.
    echo Beispiel: hash.bat C:\Pfad\zur\Datei.txt
    exit /b 1
)

:: Hash-Algorithmus (ändern Sie dies, wenn ein anderer Algorithmus verwendet werden soll)
set "hashAlgorithm=SHA256"

:: Zeilennummer-Tracker
set "lineNumber=0"

:: Berechne den Hashwert der Datei und lese die Ausgabe
for /f "tokens=*" %%A in ('certutil -hashfile "%filePath%" %hashAlgorithm%') do (
    set /a lineNumber+=1
	::timeout /t 1
    if !lineNumber! equ 2 (
        set "hashValue=%%A"
    )
)
:: Ausgabe des Hashwertes in die Datei hashlog
echo %hashValue%>> %dirNomoProfSwitcher%\Hashlog

:: #################################################################################
:: Setze Name f�r Nomo-Programmdatei
set nomoExe=nomo_app.exe
goto a
:: Kontrolle der Variablenwerte (Auskommentieren wenn fertig, oder Pause entfernen)
echo Variablenwerte:
echo
echo dirNomo:             %dirNomo%
echo dirNomoAPP:          %dirNomoAPP%	
echo dirNomoProfSwitcher: %dirNomoProfSwitcher%
echo nomoUser:            %nomoUser%
echo nomoProfs:           %nomoProfs%
echo nomo exe:            %nomoExe%
echo startPath2Nomo:      %startPath2Nomo%
echo rename zeile:        %workdir%\%dirNomo% "%workdir%\%dirNomoProfileFrom% %LastUser%"
echo dirnomoapp\dirnomo:  %dirNomoAPP%\%dirNomo%
echo renamerzeile:        %dirNomoAPP%\%dirNomo% "%dirNomoAPP%\%dirNomoProfileFrom% %LastUser%"
::Pause
::######################################################################################################################################
:a
cls
::Pr�fe ob Datei path2nomoApp.conf existiert
if exist "%startPath2Nomo%" (
	goto weiter
) else (
	:: wenn nicht existent erzeuge diese mit nachfolgender Sprungmake
	goto findPath2NomoApp
)
:findPath2NomoApp
:: Ermittle Pfad zur nomo_app.exe
@mode con cols=100 lines=%Lines%
echo.
echo    #############################################################################################
echo    #                                                                                           #
echo    #    Ermittle Pfad zur Nomo-Desktop-App in Ihrem Nutzerverzeichnis.                         #
echo    #    Bitte einen Moment Geduld, dies kann, abhaengig von der Greosse des Datenvolumes       #
echo    #    Ihres Nutzerverzeichnisses etwas Zeit in Anspruch nehmen ....                          #
echo    #                                                                                           #
echo    #############################################################################################
set gesuchte_datei=%nomoExe%
dir /s /b "%Userprofile%\%gesuchte_datei%" > %startPath2Nomo%

::Optionsdatei anlegen mit ensprechenden w�hlbaren Optionen für das spätere Auswahlmenu
echo Weiteres Profil anlegen> %switcherOptions%
echo Wiederherstellen des Originalprofils>> %switcherOptions%
echo Beenden>> %switcherOptions%

::######################################################################################################################################
:weiter
:: Setze ermittelten Pfad zur NomoApp
set /p startNomo=<%startPath2Nomo%
:: pr�fe ob Variable %startNomo% leer ist.
if "%startNomo%"=="" (
goto Error1
)
:: prüfe ob Datei existiert, wenn nicht erzeugen und mit 1 befüllen.
if exist "%dirNomoProfSwitcher%\notEmpty.nom" (
	set /p empty=<%dirNomoProfSwitcher%\notEmpty.nom
	) else if !empty! equ 1 (
		goto setlocal
		) else (
		echo.
		echo    Pfad zur NomoApp wurde erfolgreich ermittelt
		timeout /t 3 > NUL
		echo 1 > %dirNomoProfSwitcher%\notEmpty.nom
		cls
		)
)

:setlocal
setlocal enabledelayedexpansion
:: Pruefen, ob das Programm nomo.exe laeuft. Wenn ja, wird es beendet.
:: Somit entstehen beim Profilwechsel keine Konflikte.
tasklist /FI "IMAGENAME eq %nomoExe%" 2>NUL | find /I /N "%nomoExe%">NUL
if "%ERRORLEVEL%"=="0" taskkill /F /IM %nomoExe%
::echo prueft grade ob der prozess laeuft
::pause
cls
::################################### Pruefen, ob bereits eine Datei namens nomoProfiles.nom existiert. ################################
:: WICHTIG! In einem If-Block kann in Batch kein Wert zu einer Variable durch set /p zugewiesen werden, da die Variable erst beim verlassen
:: 			des If-Blockes aktualisiert bzw. geparst wird. Daher set p/ in If-Blocks meiden!!!

:: wenn die profile-datei nicht existent, dann erzeuge diese sonst prüfe existens der Datei des last users
if not exist "%nomoProfs%" (
	goto CreateProfiles
) else (
	goto existLastUser
)
::######################################################################################################################################
::######################################### Erstellen der Profilordner und ben�tigten Dateien. #########################################
:CreateProfiles
@mode con cols=94 lines=6
	:: Frage nach der Anzahl der zu erstellenden Nutzer und deren Namen.
	echo.
	echo    ##########################################################################################
	echo       Geben Sie die Anzahl der %neuen%Leute ein, fuer die ein Profil angelegt werden soll.
	echo       Danach druecken Sie die Entertaste.
	echo.   ##########################################################################################
	set /p UserCount="Ihre Eingabe: "
	echo.

	REM Leeres Array erstellen
	set "mein_array="
	
	:: Eingabe-Schleife f�r Nutzernamen (Frei w�hlbar) bzw. abhängig von Anzahl der neuen Profile.
	for /l %%i in (1,1,%UserCount%) do (
	set "eingabe="
@mode con cols=81 lines=5
	echo.
	echo ##########################################################################
	echo.
	set /p eingabe="    Geben Sie bitte nun den Namen des %%i Nutzerprofiles ein: "
	echo.
	echo ##########################################################################
	
	:: Element zum Pseudo-Array hinzufuegen
	set "mein_array=!mein_array!%!eingabe!%"
	:: erzeugt, falls noch nicht vorhanden, die nomoProfiles.nom
	echo !eingabe!>>%nomoProfs%
	:: Erstelle das Nomo-Profileverzeichnis.
	mkdir "%dirNomoAPP%\%dirNomoProfileFrom% !eingabe!"
	)
	@mode con cols=80 lines=39
	cls
	goto BackupNomoOrig
	::goto Auswahlmenu
::pause
::endlocal

::######################################## Pr�fung und Auslesen der nomoLastUser.nom ################################################################
:existLastUser
::pr�fe als erstes ob eine datei nomoLastUser.nom existiert
::echo hier im abschnitt LastUser
::pause
if exist "%nomoUser%" (
    goto readLastUser
) else (
    :: Das urspr�ngliche Profil wird gesichert, usw.
:BackupNomoOrig	
	move %dirNomoAPP%\%dirNomo% %dirNomoAPP%\%dirNomo%_Originalprofil
	
	cls

	:: Dann hier die Sprungmarke zum Auswahlmen� setzen
    goto Auswahlmenu
	
)
::#######################################
:readLastUser
::pause
:: In diesem Abschnitt wird sichergestellt das der aktuell benutzte User sein Profil beh�lt,
:: indem der zuletzt aktive Nutzer anhand der nomoLastUser.nom ermittelt wird.
:: Im Anschluss benennt das Skript das "nomo_app" Verzeichnis um zu "nomo_app Profil von %zuletzt aktiver Nutzer aus der nomoLastUser.nom%"
::
:: Beispiel:
:: aus "nomo_app" wird "nomo_app Profil von Tante Beispiel"
:: Auslesen der nomoLastUser.nom
set /p LastUser=<%nomoUser%

goto warte

:lastUSer
cls
@mode con cols=80 lines=40
echo.
echo    ########################################################
echo.
echo        Zuletzt verwendetes Profil war von: %LastUser%
echo.
echo    ########################################################

::dir %dirNomoAPP%
::echo Zuletzt verwendetes Verzeichnis umbennant
::pause
::exit

::addNewProfileBehind
:: weitere Profile im nachtr�glich hinzuf�gen


:Auswahlmenu

:: Gibt dem Benutzer ein Auswahlmenue vor.
setlocal enabledelayedexpansion
::@mode con cols=80 lines=%Lines%

REM Pr�fe, ob die Datei existiert
::if not exist "%nomoProfs%" (
::    echo Die Datei %nomoProfs% existiert nicht.
::    exit /b
::)
REM Ausgabe des Dateiinhalts mit Nummern
echo. 
echo    ########################################################################
echo    #                                                      
echo    #   Folgende Profile und Optionen stehen zur Auswahl ...
echo    #   (Scrollen Sie, wenn nicht alles sichtbar ist, oder ziehen Sie das
echo    #   Fenster etwas groesser.)
echo    #
echo    ========================================================================
echo      ---------------------------- Profile ---------------------------------
echo    ========================================================================
set "Num=1"
for /f "tokens=*" %%i in (%nomoProfs%) do (
    echo      !Num!. Profil: %%i
    set /a Num+=1
)
:: Zur�stzliche Auswahl von Optionen
echo    ========================================================================
echo      ---------------------------- Optionen --------------------------------
echo    ========================================================================
set /a Num1=1
::set "Num2="
for /f "tokens=*" %%i in (%switcherOptions%) do (
    echo      o!Num1!. Option: %%i
    set /a Num1+=1
)
echo    ========================================================================
:: Anzeige der Eintr�ge korrigieren, da beim erstellen der Profilnamen am der Zeile ein Zeilenumbruch zugef�gt wird
:: welches das Ergebnis verf�lscht.
set "korrektur=%Num%"
set "minus1=1"
set /a korrektur-=minus1

set "korrektur1=%Num1%"
set "minus1=1"
set /a korrektur1-=minus1

REM Benutzer auffordern, eine Nummer auszuw�hlen
echo    #
echo    #   Waehlen Sie Anhand der Nummern (1-%korrektur%) aus, fuer wessen
echo    #   Profil ein neues Wallet eingerichtet, oder ein bestehendes
echo    #   wiederhergestellt bzw. geoeffnet werden soll.
echo    #
echo    #   Oder waehlen Sie ueber (o1-o%korrektur1%) zu welcher Option
echo    #   gesprungen werden soll.
echo    #
echo    #   Druecken Sie nach der Eingabe die Entertaste.
echo    #
echo    ##########################################################################
echo.

::choice /c 12345678 /n /m "Waehle eine Option: "
::set "choice=%errorlevel%"

set /p "Auswahl=Was darf es sein? Die Nr. (1-%korrektur%) oder eine Optionsnr. (o1-o%korrektur1%): "
::echo %Auswahl%
::pause

if "!Auswahl!"=="" ( :: pr�fe ob Eingabe leer ist
	echo    .
	echo    Leer ist keine gueltige Eingabe
	timeout /t 3
	cls
	goto lastUSer
) else if !Auswahl! equ 0 (
	echo    .
	echo    0 ist keine gueltige Eingabe
	timeout /t 3
	cls
	goto lastUSer
) else if !Auswahl! gtr 0 (
	goto ausfuehren
) else (
	goto lastUSer
)


:ausfuehren

:: Fallunterscheidung welche Eingabe gemacht wurde (Optionsnummer oder Profilnummer?)
set "Eingabe=%Auswahl:~0,1%"
if "%Eingabe%"=="o" (
	::wenn eingabe eine Option (ein o das erste extrahierte zeichen war, dann gehe zu option
	echo gehe zu Option
	::pause
	goto Option
) else (
	::sonst gehe zu nomostart
	goto nomostart
)
exit

::Option 1
:: Neue(s) Profil(e) anlegen
:Option1
cls
set "neuen=neuen "
goto CreateProfiles


::Option 2
:Option2
cls
@mode con cols=100 lines=10
echo.
echo     Sobald Sie eine belieb. Taste drücken, wird das Original-NomoProfil wiederhergestellt
echo     und das Skript wird beendet.
echo     Sollte diese Option nur Versehendlich aufgerufen wurden sein, schliessen einfach wie
echo     gewohnt dieses Fenster mit einem klick auf das X oben Rechts. Somit wird dieser Vorgang
echo     der Option beendet.
echo.
pause > NUL
move "%dirNomoAPP%\%dirNomo%_Originalprofil" "%dirNomoAPP%\%dirNomo%"
del /F /Q "%nomoUser%"
exit

::Option 3: Beende Skript
:Option3
exit


:: Die ausgew�hlte Option finden und aufrufen
:Option
:: extrahiere 2tes zeichen des Eingabestrings des Nutzers
set "Auswahl2=%Auswahl:~1,2%"
set "ZeileNr=1"
:: Diese for-Schleife durchsucht die options-Datei um die Eingabe zu finden die der Nutzer gewählt hat.
:: Bei Übereinstimmung wird zur entsprechenden Option gesprungen.
for /f "tokens=*" %%i in (%switcherOptions%) do (
::pause
	if "%Auswahl2%"=="!ZeileNr!" (
		echo Ausgewaehlter Eintrag: %%i
		echo hei�t gehe zu:  Option %Auswahl2%
		
		endlocal
		goto Option%Auswahl2%
	)			
	
		::goto lastUSer
			::echo los in 2 sek.
			::timeout /t 2 > NUL
			::start "" %startNomo%
			::exit
			::goto :Ende	
	set /a ZeileNr+=1
)


:nomostart
:: Abschnitt in dem die nomo gestartet wird.

:: Den ausgew�hlten Eintrag finden und anzeigen und entsprechendes Profil umbenennen sowie anschließend die Nomo starten.
set "ZeileNr=1"

for /f "tokens=*" %%i in (%nomoProfs%) do (
    if "!ZeileNr!"=="%Auswahl%" (
        echo Ausgewaehlter Eintrag: %%i
		::pause
        ::ren "%dirNomoProfileFrom% %%i" "%dirNomo%"
		move "%dirNomoAPP%\%dirNomoProfileFrom% %%i" "%dirNomoAPP%\%dirNomo%"
		echo %%i>"%nomoUser%"
		endlocal
		::pause
		::echo Kontrolle: %startNomo%
		::echo los in 2 sek.
		::timeout /t 2 > NUL
		start "" %startNomo%
		exit
		goto :Ende
    )
    set /a ZeileNr+=1
)

:warte
cls
echo.
echo    Einen kleinen Moment bitte! ...
:: Warte um einen Freigabekonflikt des Dateisystems in Windows zu vermeiden.
:: Wird dieser Teil auskommentiert, kann der Zugriff auf den
:: umzubenennenden Ordner verweigert werden, da der Zugriff durch die NomoApp auf dieses
:: Verzeichnis von Windows noch nicht frei gegeben wurde.

:: Das f�hrt unter Umständen dazu, da� das gerade geladene Profil im Verzeichnis "nomo_app"
:: nicht den Namen des zuletzt ge�ffneten Nutzerprofiles zum Verzeichnisnnamen zuf�gen kann.
:: Dies wird mit der Meldung: Zugriff verweigert quittiert

:: Dieser Befehl verhindert dies
timeout /t 2 > NUL

:: Benenne das nomo_app Verzeichnis um zu
:: "nomo_app Profil von %zuletzt aktiver Nutzer aus der nomoLastUser.nom% ausgelesen"

:: Zum Umbenennen der Verzeichnisse move statt ren verwenden!
:: ren verursachte Fehler! Move l�uft zuverl�ssig!

move "%dirNomoAPP%\%dirNomo%" "%dirNomoAPP%\%dirNomoProfileFrom% %LastUser%" > NUL
goto lastUser

:Error1
:: Diese Meldung tritt ein, wenn keine "nomo_app.exe" im Nutzerverzeichnis auffindbar ist!
:: Nutzer bekommt die untenstehende Info und wird weitergeleitet
@mode con cols=94 lines=10
echo.
echo    NomoApp nicht gefunden !
echo.
echo    Bitte laden Sie die aktuelle Version herunter und entpacken diese in Ihrem
echo    Downloadordner und starten Sie dieses Skript erneut.
echo.
echo    Sobald Sie eine beliebige Taste druecken, werden Sie ueber Ihren Standardbrowser zur
echo    Download-Seite (https://nomo.app/downloads) der Nomo geleitet, wo Sie die aktuellste
echo    Version runterladen koennen.
pause > NUL
start https://nomo.app/downloads
exit

:Ende
pause
exit